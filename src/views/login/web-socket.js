const url = 'wss://shiku.liwenhao2016.cn:443/ws/'

export default {
  init: (id, fun) => {
    if (typeof (WebSocket) == "undefined") {
      alert('当前浏览器不支持消息提醒！');
    } else {
      let socket = new WebSocket(url + id);
      socket.onopen = function () {
        console.log("websocket已打开");
      };
      //获得消息事件
      socket.onmessage = function (msg) {
        fun(msg.data)
        console.log("收到服务端信息", msg.data);
      };

      //关闭事件
      socket.onclose = function () {
        console.log("websocket发生了关闭！");
      };
      socket.onerror = function () {
        console.log("websocket发生了错误");
      }
    }
  }
}

