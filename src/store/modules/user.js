import {setToken, removeToken} from '@/utils/auth'
import {resetRouter} from '@/router'

const getDefaultState = () => {
  return {
    token: null,
    name: '',
    shop: {}
  }
}

const state = getDefaultState()

const mutations = {
  RESET_STATE: (state) => {
    Object.assign(state, getDefaultState())
  },
  SET_TOKEN: (state, token) => {
    state.token = token
  },
  SET_SHOP: (state, shop) => {
    state.shop = shop
  }
}

const actions = {
  // user login
  login({commit}, shop) {
    commit('SET_SHOP', shop)
    setToken(shop.id)
  },

  // get user info
  getInfo({commit, state}) {
  },

  // user logout
  logout({commit, state}) {
    removeToken() // must remove  token  first
    resetRouter()
    commit('RESET_STATE')
  },

  resetToken({commit}) {
    removeToken() // must remove  token  first
    commit('SET_TOKEN', null)
  }
}

export default {
  namespaced: true,
  state,
  mutations,
  actions
}

